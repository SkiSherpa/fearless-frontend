# Title of app - fill me in

A project exploring the data synchronization through polling, queues, and pub/sub to connect data flowing between a monolith and various microservices. Through this project you can create and attend conferences.


## How to Run this App

*Requirements*
- Docker-compose (The application depends on building out the services into separate containers)
- Node JS
- Git

## How to install and use
1. Fork and Clone the repository
2. Create a directory on your local machine that you would like the application located in.
3. Be in the directory you want the application in and use the command:
    ```
     git clone <Link to your clone of the application here>
     ```
4. Use the following commands in the order specified to start up Docker:
    ```
    docker volume create pgdata
    docker-compose build
    docker-compose up
    ```
5. Building and running the application can take some time. Wait for all your docker containers to be running before trying to use the app. Maybe grab some coffee, or pizza?

6. In your browser, navigate to http://localhost:3001/
7. Create a location at the "New Locations" tab.
8. Create a conference at the "New Conference" tab.
9. Create a presentation at the "New Presentation" tab.
