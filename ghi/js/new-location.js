// add event listener for the dom

// declare var to hold URL for the API we just created

// Fetch the URL to await the response

// If the response is ok,
// get the data using .json method - don't forget await

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/states/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error('Response not ok');
    } else {
      const data = await response.json();
      // loop thro "states"
      // create an option tag,
      // set the current state as inner html to id="state"
      for (let state of data.states) {
        const newOptionElement = document.createElement('option');
        newOptionElement.value = state.abbreviation;
        newOptionElement.innerHTML = state.name;
        const stateSelect = document.querySelector('#state');
        stateSelect.appendChild(newOptionElement);
      }
    }
  } catch (e) {
    console.error('Error has been caught');
  }

  const formTag = document.getElementById('create-location-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    // convert form data to JSON to call to the API
    // similar to promise to data you can work with
    // seems like form data is another data-type
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData))
    // console.log(json);
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();

    }
  })

})
