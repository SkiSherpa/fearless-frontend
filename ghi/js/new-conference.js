window.addEventListener('DOMContentLoaded', async () => {
  // to populate the choose location dropdown
  // url should be all the possible locations
  const url = 'http://localhost:8000/api/locations/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error('Response not ok')
    } else {
      const data = await response.json();
      // console.log(data);
      for (let location of data.locations) {

        const newOptionElement = document.createElement('option');
        newOptionElement.value = location.id; // id
        newOptionElement.innerHTML = location.name;
        const locationSelect = document.querySelector('#location');
        locationSelect.appendChild(newOptionElement);
      }
    }
  } catch (e) {
    console.error('Error has been caught in getting list of locations')
  }

  const formTag = document.getElementById('create-conference-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    // console.log(json);
    const conferenceURL = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(conferenceURL, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
    }
  })
})
