function createCard(name, decsription, picture_url, startDate, endDate, location) {
  return `
    <div class="card pt-3 px-3 shadow p-3 mb-5 bg-body rounded">
      <img src="${picture_url}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="location text-muted">${location}</p>
        <p class="card-text">${decsription}</p>
        <footer class="card-footer text-muted">${startDate} - ${endDate}</footer>
      </div>
    </div>
  `;
}

function alert() {
  return '<div class="alert alert-warning" role="alert">Could not load upcoming conferences :( </div>';
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      console.error("Error for response");
      const connectAlert = alert();
      const containerError = document.querySelector("#alertPlaceHolder")
      containerError.innerHTML += connectAlert;
    } else {
      // if response is good.
      const data = await response.json();
      console.log(data.conferences);
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log('details: ', details);
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = new Date(details.conference.starts);
          const ends= new Date(details.conference.ends);
          const startDate = starts.toString().slice(4, 15);
          const endDate = ends.toString().slice(4, 15);
          const location = details.conference.location.name;

          const html = createCard(title, description, pictureUrl, startDate, endDate, location);
          // "html" has card data ^^
          // grab the num from the href in "conference"
          // if <= 3 assign that "html" var to that col
          // otherwise, use % to place in respective col
          const colNumber = data.conferences.indexOf(conference) + 1;
          if (colNumber <= 3) {
            // selecting the div with class col
            const column = document.querySelector(`.col${colNumber}`);
            // placement of "html" into a col
            column.innerHTML += html;
          } else {
            const remainderCol = colNumber % 3;
            const column = document.querySelector(`.col${remainderCol}`);
            column.innerHTML += html;
          }
        }
      }
    }
  } catch (e) {
    console.error("Error has been caught");
    const connectAlert = alert();
    const containerError = document.querySelector("#alertPlaceHolder")
    containerError.innerHTML = connectAlert;
  }
});
