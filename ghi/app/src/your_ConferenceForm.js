import React, { useEffect, useState } from "react";


function ConferenceForm(props) {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [description, setDescription] = useState("");
  const [maxPresentations, setMaxPresentations] = useState("");
  const [maxAttendees, setMaxAttendees] = useState("");
  const [location, setLocation] = useState("");


  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleStartDateChange = (event) => {
    const value = event.target.value;
    setStartDate(value);
  }

  const handleEndDateChange = (event) => {
    const value = event.target.value;
    setEndDate(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaxPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }

  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const fetchData = async () => {
    // pull the data from locations to be fed into creating a conference
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.name = name;
    data.starts = startDate;
    data.ends = endDate;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.location = location;
    // console.log(data);

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      }
    };

    const response = await fetch(conferenceUrl, fetchConfig)
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form
            onSubmit={handleSubmit} id="create-conference-form"
          >
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <label htmlFor="starts">Starts</label>
              <input
                onChange={handleStartDateChange}
                required
                type="date"
                name="starts"
                id="starts"
                className="form-control"
              />
            </div>

            <div className="form-floating mb-3">
              <label htmlFor="ends">Ends</label>
              <input
                onChange={handleEndDateChange}
                required
                type="date"
                name="ends"
                id="ends"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Description
              </label>
              <textarea
                onChange={handleDescriptionChange}
                className="form-control"
                id="description"
                rows="3"
              ></textarea>
            </div>

            <div className="form-floating mb-3">
              <label htmlFor="max-presentations">Maximum presentations</label>
              <input
                onChange={handleMaxPresentationsChange}
                required
                type="number"
                name="max_presentations"
                id="max-presentations"
                className="form-control"
              />
            </div>

            <div className="form-floating mb-3">
              <label htmlFor="max-attendees">Maximum attendees</label>
              <input
                onChange={handleMaxAttendeesChange}
                required
                type="number"
                name="max_attendees"
                id="max-attendees"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <select
                onChange={handleLocationChange}
                required
                id="location"
                name="location"
                className="form-select"
              >
                <option value="">
                  Choose a location
                </option>
                {locations.map((location) => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ConferenceForm;
