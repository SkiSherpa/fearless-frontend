// import '/App.css';
import Nav from './Nav.js';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm.js';
import ConferenceForm from './ConferenceForm.js';
import AttendConferenceForm from './AttendConferenceForm.js';
import PresentationForm from './PresentationForm.js';
import MainPage from './MainPage.js';
import { BrowserRouter, Routes, Route, } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" index element={<MainPage conferences={props.conferences}/>} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conference">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendeesList />} />
          </Route>
          <Route path="attendconferenceform">
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="presentation">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
